package sockets.tcp;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Arrays;

import com.vinicius3w.cripto.Criptografia;

public class TCPClient {
	public static void main(String argv[]) throws Exception {
		String sentence;
		String modifiedSentence;
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
		while(true){
			System.out.println("Digite algo para enviar ao servidor TCP...");
			//Abre conex„o com destino: local e porta: 6789
			Socket clientSocket = new Socket("localhost", 6789);
			//LÍ entrada do usu·rio.
			sentence = inFromUser.readLine();
			//Cria canal de comunicaÁ„o com o servidor
			DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
			
			Criptografia criptografia = new Criptografia();
			byte[] data = criptografia.encrypt(sentence);
			
			System.out.println("Dados cifrados " + Arrays.toString(data));
			
			int len = data.length;
			//Enviar a mensagem ao servidor.
			outToServer.writeInt(len);
			if(len > 0){
				outToServer.write(data, 0, len);
			}
			
			//LÍ resposta do servidor.
			BufferedReader inFromServer = new BufferedReader(new InputStreamReader(
					clientSocket.getInputStream()));
			modifiedSentence = inFromServer.readLine();
			System.out.println("Recebido do servidor TCP: " + modifiedSentence);
			clientSocket.close();
		}
	}
}