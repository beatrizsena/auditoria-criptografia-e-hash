package sockets.tcp;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import com.vinicius3w.cripto.Criptografia;

public class TCPServer {
	private static ServerSocket welcomeSocket;

	public static void main(String argv[]) throws Exception {
		String clientSentence;
		String capitalizedSentence;
		
		welcomeSocket = new ServerSocket(6789);
		
		while (true) {
			System.out.println("Servidor TCP ouvindo...");
			//Aceitando conexıes de clientes.
			Socket connectionSocket = welcomeSocket.accept();
			
			//Lendo dados recebidos.
			BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
			
			//Abrindo canal de comunicaÁ„o para escrita no socket.
			DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
			
		    InputStream in = connectionSocket.getInputStream();
		    DataInputStream dis = new DataInputStream(in);			
			
		    int len = dis.readInt();
		    byte[] data = new byte[len];
		    if (len > 0) {
		    	dis.readFully(data);
		    }
		    
		    Criptografia criptografia = new Criptografia();
		    System.out.println("Dados descriptografados: " + criptografia.decrypt(data));
		    
			//clientSentence = inFromClient.readLine();
			
//			System.out.println("Received: " + clientSentence);
			//A resposta ser· a mesma mensagem, porÈm captalizada.
//			capitalizedSentence = clientSentence.toUpperCase() + '\n';
//			outToClient.writeBytes(capitalizedSentence);
		}
	}
}